package com.example.thesis.thesisbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThesisBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThesisBackEndApplication.class, args);
	}

}
