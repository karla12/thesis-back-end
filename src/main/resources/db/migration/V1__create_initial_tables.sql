CREATE TABLE IF NOT EXISTS "user"
(
    id            BIGSERIAL   NOT NULL,
    first_name    VARCHAR(45) NULL,
    last_name     VARCHAR(45) NULL,
    personal_code VARCHAR(45) NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS "product"
(
    id            BIGSERIAL   NOT NULL,
    name          VARCHAR(255) NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user_has_product
(
    id              BIGSERIAL    NOT NULL,
    user_id         INT          NOT NULL,
    product_id      INT          NOT NULL,
    created_at      TIMESTAMP(0) NULL,
    modified_at     TIMESTAMP(0) NULL,
    deleted_at      TIMESTAMP(0) NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_user_has_product_has_user_id
        FOREIGN KEY (user_id)
            REFERENCES "user" (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT fk_user_has_product_has_product_id
        FOREIGN KEY (product_id)
            REFERENCES "product" (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
);